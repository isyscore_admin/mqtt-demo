# 项目描述

此项目基于MQTT协议，实现client与broker通讯的示例，**【特别注意】此工程仅供开发者参考使用，若用于生产环境请自行评估风险**

# 技术栈
- [spring boot 2.0.9.RELEAS](https://spring.io/projects/spring-boot/)
- 指令集SDK isyscore-boot-starter-mqtt 1.0.1-SNAPSHOT

# 背景描述
- 基于MQTT协议并通过直连方式，将物联网设备接入到指令集物联网平台

# 功能描述
- 设备端数据定时上报指令集物联网平台(代码实现参考com.isyscore.os.App.mockMqttProducer)
- 设备端订阅指令集物联网平台下发的指令(代码实现参考com.isyscore.os.App.init)

# 使用说明
- 在resources/application中配置MQTT设备身份认证信息(mqtt.server-uri、mqtt.client-id、mqtt.password)
- 在resources/application中配置设备端上传数据topic(to-os-topic)，配置订阅平台下发指令的topic(subscribe-os-topic)
- 上传的报文请自行在代码com.isyscore.os.App.mockMqttProducer中修改