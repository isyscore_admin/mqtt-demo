package com.isyscore.os.mqtt;

import com.isyscore.boot.mqtt.ReceivedMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * mqtt消费逻辑
 */
@Component
@Slf4j
public class MqttCustomer {

    public void deal(ReceivedMessage message) {
        if (null == message || null == message.getPayload()
                || 0 == message.getPayload().length) {
            return;
        }
        String s = new String(message.getPayload());
        log.info(">>>收到os平台下发数据:{}",s);
    }
}
